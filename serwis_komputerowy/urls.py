from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'serwis_komputerowy.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),

                       url(r'^grappelli/', include('grappelli.urls')),
                       url('', include(admin.site.urls)),
                       )
