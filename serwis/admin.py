from django.contrib import admin

# Register your models here.

# https://docs.djangoproject.com/en/1.7/topics/auth/customizing/#extending-the-existing-user-model
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from serwis.models import *


class PracownikInline(admin.TabularInline):
    model = Pracownik
    can_delete = False
    verbose_name_plural = 'pracownicy'


class UserAdmin(UserAdmin):
    inlines = (PracownikInline, )
    # https://docs.djangoproject.com/en/1.7/topics/auth/customizing/#a-full-example
    add_fieldsets = ((None,
                      {'fields': ('username',
                                  'first_name',
                                  'last_name',
                                  'email',
                                  'password1',
                                  'password2',
                                  'groups',
                                  'is_staff')}),
                     )
#    list_display = ['username', 'first_name', 'last_name', 'email', 'aktualna_pensja']

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
# ##


class KlientAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Klient._meta.fields]


class SprzetAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Sprzet._meta.fields]


class FakturaAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Faktura._meta.fields]


class NaprawaAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Naprawa._meta.fields]


class FirmaAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Firma._meta.fields]


class UslugaDodatkowaAdmin(admin.ModelAdmin):
    list_display = [f.name for f in UslugaDodatkowa._meta.fields]


class WysylkaAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Wysylka._meta.fields]


admin.site.register(Klient, KlientAdmin)
admin.site.register(Sprzet, SprzetAdmin)
admin.site.register(Faktura, FakturaAdmin)
admin.site.register(Naprawa, NaprawaAdmin)
admin.site.register(Firma, FirmaAdmin)
admin.site.register(UslugaDodatkowa, UslugaDodatkowaAdmin)
admin.site.register(Wysylka, WysylkaAdmin)
