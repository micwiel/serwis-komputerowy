# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Faktura',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('data_wystawienia', models.DateTimeField(default=datetime.datetime(2015, 1, 6, 16, 33, 11, 595471))),
                ('kwota', models.PositiveIntegerField()),
                ('kontrahent_id', models.PositiveIntegerField()),
                ('usluga_id', models.PositiveIntegerField()),
                ('typ_kontrahenta', models.ForeignKey(to='contenttypes.ContentType', related_name='kontrahent')),
                ('typ_uslugi', models.ForeignKey(to='contenttypes.ContentType', related_name='usluga')),
            ],
            options={
                'verbose_name_plural': 'faktury',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Firma',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('nazwa', models.CharField(max_length=50)),
                ('nr_telefonu', models.CharField(max_length=15)),
                ('adres', models.TextField()),
            ],
            options={
                'verbose_name_plural': 'firmy',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Klient',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('imie', models.CharField(max_length=30)),
                ('nazwisko', models.CharField(max_length=30)),
                ('email', models.EmailField(max_length=75)),
                ('nr_telefonu', models.CharField(max_length=15)),
                ('adres_zamieszkania', models.TextField()),
            ],
            options={
                'verbose_name_plural': 'klienci',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Naprawa',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('usterka', models.TextField()),
                ('przyczyna_usterki', models.TextField()),
                ('wycena_naprawy', models.PositiveIntegerField()),
                ('status', models.PositiveSmallIntegerField(choices=[(1, 'oczekuje'), (2, 'w naprawie'), (3, 'naprawiono')])),
                ('data_zgloszenia', models.DateTimeField(default=datetime.datetime(2015, 1, 6, 16, 33, 11, 597359))),
                ('data_naprawy', models.DateTimeField(null=True, blank=True)),
                ('data_zwrotu', models.DateTimeField(null=True, blank=True)),
                ('kontrahent_id', models.PositiveIntegerField()),
            ],
            options={
                'verbose_name_plural': 'naprawy',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Pracownik',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('nr_telefonu', models.CharField(max_length=15)),
                ('adres_zamieszkania', models.TextField()),
                ('data_urodzenia', models.DateField()),
                ('wyksztalcenie', models.CharField(max_length=20)),
                ('aktualna_pensja', models.PositiveIntegerField()),
                ('zdjecie', models.ImageField(upload_to='', blank=True)),
                ('od_kiedy_pracownik', models.DateTimeField(default=datetime.datetime(2015, 1, 6, 16, 33, 11, 593334))),
                ('do_kiedy_pracownik', models.DateTimeField(null=True, blank=True)),
                ('django_user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sprzet',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('nazwa', models.CharField(max_length=30)),
                ('marka', models.CharField(max_length=20, blank=True)),
                ('typ', models.PositiveSmallIntegerField(choices=[(1, 'komputer'), (2, 'laptop'), (3, 'telefon'), (4, 'inne')])),
                ('rocznik', models.DateField(null=True, blank=True)),
                ('wartosc', models.PositiveIntegerField()),
            ],
            options={
                'verbose_name_plural': 'sprzęty',
                'verbose_name': 'sprzęt',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UslugaDodatkowa',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('nazwa', models.CharField(max_length=30)),
                ('rodzaj', models.CharField(max_length=20)),
                ('cena', models.PositiveIntegerField()),
                ('czas_wykonania', models.PositiveIntegerField()),
            ],
            options={
                'verbose_name_plural': 'usługi',
                'verbose_name': 'usługa',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='naprawa',
            name='sprzet',
            field=models.OneToOneField(to='serwis.Sprzet'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='naprawa',
            name='typ_kontrahenta',
            field=models.ForeignKey(to='contenttypes.ContentType'),
            preserve_default=True,
        ),
    ]
