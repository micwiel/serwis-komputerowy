# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0001_initial'),
        ('serwis', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Wysylka',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('data', models.DateTimeField(default=datetime.datetime(2015, 1, 7, 0, 44, 30, 992383))),
                ('kontrahent_id', models.PositiveIntegerField()),
                ('sprzet', models.OneToOneField(to='serwis.Sprzet')),
                ('typ_kontrahenta', models.ForeignKey(to='contenttypes.ContentType')),
            ],
            options={
                'verbose_name': 'wysyłka',
                'verbose_name_plural': 'wysyłki',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='firma',
            name='nip',
            field=models.CharField(max_length=50, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='firma',
            name='regon',
            field=models.CharField(max_length=50, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='firma',
            name='strona_www',
            field=models.CharField(max_length=50, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='firma',
            name='wlasciciel',
            field=models.CharField(max_length=50, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='pracownik',
            name='uwagi',
            field=models.TextField(blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='pracownik',
            name='zakres_obowiazkow',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='faktura',
            name='data_wystawienia',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 7, 0, 44, 30, 986245)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='naprawa',
            name='data_zgloszenia',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 7, 0, 44, 30, 988029)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='pracownik',
            name='od_kiedy_pracownik',
            field=models.DateTimeField(default=datetime.datetime(2015, 1, 7, 0, 44, 30, 983455)),
            preserve_default=True,
        ),
    ]
