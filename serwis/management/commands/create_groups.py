# https://docs.djangoproject.com/en/1.7/howto/custom-management-commands/
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = 'Creates all required groups'

    def handle(self, *args, **options):
        from django.contrib.auth.models import Group

        from django.contrib.auth.models import Permission

        g, _ = Group.objects.get_or_create(name='Pracownicy')
        models = [
            'klient',
            'sprzet',
            'faktura',
            'naprawa',
            'firma',
            'uslugadodatkowa',
            'wysylka']

        p = [
            Permission.objects.get(
                codename='change_{}'.format(x)) for x in models]
        for i in p:
            g.permissions.add(i)
        p = [
            Permission.objects.get(
                codename='add_{}'.format(x)) for x in models]
        for i in p:
            g.permissions.add(i)

        g.save()

        g, _ = Group.objects.get_or_create(name='Administratorzy')

        g.permissions.add(Permission.objects.get(codename='add_user'))
        g.permissions.add(Permission.objects.get(codename='change_user'))
        g.permissions.add(Permission.objects.get(codename='add_pracownik'))
        g.permissions.add(Permission.objects.get(codename='change_pracownik'))

        g.save()
