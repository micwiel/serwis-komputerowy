from django.db import models

# Create your models here.

from datetime import datetime
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic


class Pracownik(models.Model):
    from django.contrib.auth.models import User
    django_user = models.OneToOneField(User)
    nr_telefonu = models.CharField(max_length=15)
    adres_zamieszkania = models.TextField()
    data_urodzenia = models.DateField()
    wyksztalcenie = models.CharField(max_length=20)
    aktualna_pensja = models.PositiveIntegerField()
    zdjecie = models.ImageField(blank=True)
    od_kiedy_pracownik = models.DateTimeField(default=datetime.now())
    do_kiedy_pracownik = models.DateTimeField(blank=True, null=True)
    zakres_obowiazkow = models.TextField()
    uwagi = models.TextField(blank=True)


TYP_SPRZETU = ((1, 'komputer'), (2, 'laptop'), (3, 'telefon'), (4, 'inne'),)


class Sprzet(models.Model):
    nazwa = models.CharField(max_length=30)
    marka = models.CharField(max_length=20, blank=True)
    typ = models.PositiveSmallIntegerField(choices=TYP_SPRZETU)
    rocznik = models.DateField(blank=True, null=True)
    wartosc = models.PositiveIntegerField()

    def __str__(self):
        return "{} {}".format(self.nazwa, self.marka)

    class Meta:
        verbose_name = "sprzęt"
        verbose_name_plural = "sprzęty"


class Faktura(models.Model):
    data_wystawienia = models.DateTimeField(default=datetime.now())
    kwota = models.PositiveIntegerField()

    limit = models.Q(
        app_label='serwis',
        model='klient') | models.Q(
            app_label='serwis',
            model='firma')
    typ_kontrahenta = models.ForeignKey(
        ContentType,
        related_name='kontrahent',
        limit_choices_to=limit)
    kontrahent_id = models.PositiveIntegerField()
    kontrahent = GenericForeignKey('typ_kontrahenta', 'kontrahent_id')

    limit = models.Q(
        app_label='serwis',
        model='naprawa') | models.Q(
            app_label='serwis',
            model='uslugadodatkowa')
    typ_uslugi = models.ForeignKey(
        ContentType,
        related_name='usluga',
        limit_choices_to=limit)
    usluga_id = models.PositiveIntegerField()
    usluga = GenericForeignKey('typ_uslugi', 'usluga_id')

    class Meta:
        verbose_name_plural = "faktury"


STATUSY_NAPRAWY = ((1, 'oczekuje'), (2, 'w naprawie'), (3, 'naprawiono'),)


class Naprawa(models.Model):
    sprzet = models.OneToOneField(Sprzet, limit_choices_to={'naprawa': None})
    usterka = models.TextField()
    przyczyna_usterki = models.TextField()
    wycena_naprawy = models.PositiveIntegerField()
    status = models.PositiveSmallIntegerField(choices=STATUSY_NAPRAWY)
    data_zgloszenia = models.DateTimeField(default=datetime.now())
    data_naprawy = models.DateTimeField(blank=True, null=True)
    data_zwrotu = models.DateTimeField(blank=True, null=True)

    limit = models.Q(
        app_label='serwis',
        model='klient') | models.Q(
            app_label='serwis',
            model='firma')
    typ_kontrahenta = models.ForeignKey(ContentType, limit_choices_to=limit)
    kontrahent_id = models.PositiveIntegerField()
    klient = GenericForeignKey('typ_kontrahenta', 'kontrahent_id')

    usluga = generic.GenericRelation(Faktura)

    class Meta:
        verbose_name_plural = "naprawy"


class Klient(models.Model):
    imie = models.CharField(max_length=30)
    nazwisko = models.CharField(max_length=30)
    email = models.EmailField()
    nr_telefonu = models.CharField(max_length=15)
    adres_zamieszkania = models.TextField()
    usluga = generic.GenericRelation(Naprawa)

    class Meta:
        verbose_name_plural = "klienci"


class Firma(models.Model):
    nazwa = models.CharField(max_length=50)
    nr_telefonu = models.CharField(max_length=15)
    adres = models.TextField()
    strona_www = models.CharField(max_length=50, blank=True)
    nip = models.CharField(max_length=50)
    regon = models.CharField(max_length=50)
    wlasciciel = models.CharField(max_length=50)
    usluga = generic.GenericRelation(Naprawa)

    class Meta:
        verbose_name_plural = "firmy"


class UslugaDodatkowa(models.Model):
    nazwa = models.CharField(max_length=30)
    rodzaj = models.CharField(max_length=20)
    cena = models.PositiveIntegerField()
    czas_wykonania = models.PositiveIntegerField()
    usluga = generic.GenericRelation(Faktura)

    class Meta:
        verbose_name = "usługa"
        verbose_name_plural = "usługi"


class Wysylka(models.Model):
    sprzet = models.OneToOneField(Sprzet, limit_choices_to={'naprawa': True})
    data = models.DateTimeField(default=datetime.now())
    limit = models.Q(
        app_label='serwis',
        model='klient') | models.Q(
            app_label='serwis',
            model='firma')
    typ_kontrahenta = models.ForeignKey(
        ContentType,
        limit_choices_to=limit)
    kontrahent_id = models.PositiveIntegerField()
    kontrahent = GenericForeignKey('typ_kontrahenta', 'kontrahent_id')

    class Meta:
        verbose_name = "wysyłka"
        verbose_name_plural = "wysyłki"
